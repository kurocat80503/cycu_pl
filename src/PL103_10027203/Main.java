import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class Main {
  public static void main( String[] args ) throws IOException {
    ( new OurScheme() ).Start();
  } // main()
} // class Main

class OurScheme {
  private BufferedReader mReader = null;
  private TokenParser mInput = null;
  private int mTestNum = -1 ;

  public OurScheme() throws IOException {
    mReader = new BufferedReader( new InputStreamReader( System.in ) ) ;
    mInput = TokenParser.GetInstance() ;
    mInput.SetReader( mReader );
    mTestNum = Integer.parseInt( mReader.readLine().trim() ) ;
  } // OurScheme()

  public void Start() {
    boolean commandExit = false ;
    boolean appearLeftComa = false ;

    Database db = new Database() ;
    db.AddPrimtiveFunction();

    try {
      System.out.print( "Welcome to OurScheme!\n\n" );
      do {
        System.out.print( "> " );
        appearLeftComa = true ;
        SyntaxTree tree = new SyntaxTree();
        try {
          if ( tree.CreateTree() ) {
            try {
              EvalResult evr = tree.Eval( db );
              commandExit = evr.GetType().equals( "exit" ) ;
              if ( !commandExit ) {
                if ( evr.IsClosure() )
                  System.out.println( "#<user function>" ) ;
                else if ( !evr.GetType().equals( "define" ) && !evr.GetType().equals( "clean-env" ) )
                  tree.Travel( evr.GetResult(), db );
                System.out.print( "\n" );
                mInput.Reset();
                appearLeftComa = false ;
              } // if
            } // try
            catch( IncorrectNumberOfArguments exp1 ) {
              System.out.println( "ERROR (incorrect number of arguments) : " +
                                  exp1.GetErrCommandName() + "\n" );
              mInput.Reset();
            } // catch
            catch( NonListException exp2 ) {
              System.out.print( "ERROR (non-list) : " ) ;
              tree.Travel( exp2.GetErrNdoe(), true, db );
              System.out.print( "\n" ) ;
              mInput.Reset();
            } // catch
            catch( AttemptToApplyNonFunction exp3 ) {
              String str = exp3.GetErrNdoe().GetVal().GetVal() ;
              System.out.println( "ERROR (attempt to apply non-function) : " + str + "\n" );
              mInput.Reset();
            } // catch
            catch( LevelOfExit exp4 ) {
              System.out.println( "ERROR (level of exit)\n" );
              mInput.Reset();
            } // catch
            catch( UnboundSymbol exp5 ) {
              System.out.println( "ERROR (unbound symbol) : " + exp5.GetErrSymbolName() + "\n" ) ;
              mInput.Reset();
            } // catch
            catch( DefineFormatException exp6 ) {
              System.out.println( "ERROR (define format)\n" ) ;
              mInput.Reset();
            } // catch
            catch( CleanEnvNotRootException exp7 ) {
              System.out.println( "ERROR (clean-environment format)\n" ) ;
              mInput.Reset();
            } // catch
            catch( IncorrectArgumentType exp8 ) {
              System.out.print( "ERROR (" + exp8.GetFuncName() + " with incorrect argument type) : " ) ;
              tree.Travel( exp8.GetErrNode(), true, db );
              System.out.print( "\n" ) ;
              mInput.Reset();
            } // catch
            catch( ParameterformatException exp9 ) {
              System.out.print( "ERROR (cond parameter format) : " ) ;
              tree.Travel( exp9.GetErrNdoe(), true, db );
              System.out.print( "\n" ) ;
              mInput.Reset();
            } // catch
            catch( NoReturnValue exp10 ) {
              System.out.println( "ERROR (no return value) : " + exp10.GetErrName() + "\n" ) ;
              mInput.Reset();
            } // catch
            catch( LetFormatException exp11 ) {
              System.out.println( "ERROR (LET format)\n" ) ;
              mInput.Reset();
            } // catch
            catch( LambdaFormatException exp12 ) {
              System.out.println( "ERROR (lambda format)\n" ) ;
              mInput.Reset();
            } // catch
            finally {
              db.ClearFunction() ;
            } // finally

            appearLeftComa = false ;
          } // if
        } catch( LineEnterEncountered lee ) {
          int line = lee.GetLine(), col = lee.GetColumn();
          System.out.println( "ERROR (unexpected character) : line " + line + " column " + col +
                              " LINE-ENTER encountered\n" );
          mInput.Reset();
        } // catch()
        catch( UnexpectedCharacterException epce ) {
          Token token = epce.GetToken();
          System.out.println( "ERROR (unexpected character) : line " + token.GetLine() + " column " +
                              token.GetCol() + " character '" + token.GetVal() + "'\n" );
          mInput.AbortLine();
          mInput.Reset();
          appearLeftComa = false ;
        } // catch()
      } while ( !commandExit && !mInput.CheckEOF() ) ;

      if ( !commandExit && mInput.CheckEOF() ) {
        if ( !appearLeftComa ) System.out.print( "> " );
        System.out.println( "ERROR : END-OF-FILE encountered when there should be more input\n" );
      } // if
    } // try
    catch( EOFException eof ) {
      if ( !appearLeftComa ) System.out.print( "> " ) ;
      System.out.println( "ERROR : END-OF-FILE encountered when there should be more input\n" );
      appearLeftComa = false ;
    } // catch
    catch( Throwable exp ) {
      exp.printStackTrace();
    } // catch

    System.out.print( "Thanks for using OurScheme!" );
  } // Start()
} // class OurScheme

class TokenParser {
  private BufferedReader mReader = null ;
  private boolean mInputEnded = false;
  private int mLine = 1, mColumn = 0;
  private int mLeftParenCount = 0 ;

  private char mCurCh = '\0' ;

  private String mReadBuffer = null ;
  private int mBuffCurPos = 0 ;

  private static TokenParser sParser ;
  private TokenParser() {
    mReadBuffer = new String();
  } // TokenParser()

  public static TokenParser GetInstance() {
    if ( sParser == null ) sParser = new TokenParser();
    return sParser;
  } // GetInstance()

  public void SetReader( BufferedReader reader ) {
    mReader = reader ;
  } // SetReader()

  public int GetNowLine() {
    return mLine;
  } // GetNowLine()

  public int GetNowColumn() {
    return mColumn;
  } // GetNowColumn()

  public char GetNowCharacter() {
    return mCurCh;
  } // GetNowCharacter()

  public boolean CheckEOF() {
    return mInputEnded;
  } // CheckEOF()

  private boolean GetNewLine() throws Throwable {
    StringBuilder sb = new StringBuilder() ;
    int c = -1 ;
    for ( c = mReader.read() ; c != -1 && c != '\n' && c != '\r' ; c = mReader.read() ) {
      sb.append( ( char ) c ) ;
    } // for

    if ( c == '\n' || c == '\r' ) sb.append( ( char ) c ) ;
    if ( sb.length() != 0 ) {
      mReadBuffer = sb.toString() ;
      mBuffCurPos = 0 ;
      return true ;
    } // if

    mInputEnded = true ;
    return false ;
  } // GetNewLine()

  public char GetChar() throws Throwable {
    if ( mReadBuffer == null || mBuffCurPos == mReadBuffer.length() ) GetNewLine() ;
    if ( !CheckEOF() ) {
      ++mColumn;
      return mReadBuffer.charAt( mBuffCurPos++ ) ;
    } // if

    return '\0' ;
  } // GetChar()

  public Token GetNextToken() throws Throwable {
    if ( CheckEOF() ) throw new EOFException() ;

    if ( mCurCh == '\0' && !CheckEOF() ) mCurCh = GetChar();
    while ( ! CheckEOF() ) {
      if ( mCurCh == '(' || mCurCh == ')' || mCurCh == '\'' ) {

        if ( mLine == 0 ) mLine = 1 ;
        if ( mColumn == 0 ) mColumn = 1 ;

        if ( mCurCh == '(' ) mLeftParenCount++ ;
        else if ( mCurCh == ')' ) mLeftParenCount-- ;

        Token token = CreateToken( String.valueOf( mCurCh ), mLine, mColumn );
        mCurCh = '\0';
        return token;
      } // if
      else if ( mCurCh == '"' ) {
        if ( mLine == 0 ) mLine = 1 ;
        if ( mColumn == 0 ) mColumn = 1 ;

        int tokenCol = mColumn;
        String str = String.valueOf( mCurCh );
        mCurCh = GetChar();
        while ( !CheckEOF() && mCurCh != '"' && mCurCh != '\n' && mCurCh != '\r' ) {
          str += mCurCh;
          mCurCh = GetChar();
        } // while

        if ( CheckEOF() ) throw new EOFException() ;

        if ( mCurCh == '\r' || mCurCh == '\n' ) {
          mCurCh = '\0' ;
          throw new LineEnterEncountered( mLine, mColumn );
        } // if

        if ( mCurCh == '"' ) {
          str += mCurCh;
          mCurCh = '\0';
        } // else if

        if ( CheckEOF() ) return null ;
        return CreateToken( str, mLine, tokenCol );
      } // else if
      else if ( mCurCh == '.' ) {
        if ( mLine == 0 ) mLine = 1 ;
        if ( mColumn == 0 ) mColumn = 1 ;

        int loc = mColumn;

        char backward = GetChar();
        if ( CheckEOF() ) throw new EOFException();

        if ( mLeftParenCount == 0 && ( backward == '\n' || backward == '\r' ) ) {
          mCurCh = '\0';
          throw new LineEnterEncountered( mLine, mColumn );
        } // else if
        else if ( mLeftParenCount == 0 && IsSeparator( backward ) ) {
          mCurCh = '\0';
          Token tmp = CreateToken( String.valueOf( backward ), mLine, mColumn ) ;
          throw new UnexpectedCharacterException( tmp );
        } // else if
        else if ( mLeftParenCount == 0 && backward == '\0' )
          throw new EOFException() ;

        if ( IsWhiteSpace( backward ) ) {
          Token token = CreateToken( String.valueOf( mCurCh ), mLine, loc );
          mCurCh = backward ;
          return token;
        } // if
        else {
          String str = String.valueOf( mCurCh ) ;
          mCurCh = backward ;
          while ( !CheckEOF() && !IsSeparator( mCurCh ) ) {
            str += mCurCh;
            mCurCh = GetChar() ;
          } // while

          return CreateToken( str, mLine, loc );
        } // else
      } // else if
      else if ( IsWhiteSpace( mCurCh ) ) {
        if ( mColumn == 0 ) mColumn = 1 ;

        if ( mCurCh == '\n' ) {
          if ( mLine == 0 ) mLine = 1 ;
          mLine++;
          mColumn = 0;
          mCurCh = GetChar() ;
        } // if
        else if ( mCurCh == '\r' ) {
          mColumn = 0;
          mCurCh = GetChar() ;
        } // else if
        else {
          mCurCh = GetChar() ;
          while ( !CheckEOF() && ( mCurCh == ' ' || mCurCh == '\t' ) ) {
            mCurCh = GetChar() ;
          } // while
        } // else
      } // else if
      else if ( mCurCh == ';' ) {
        if ( mLine == 0 ) mLine = 1 ;
        if ( mColumn == 0 ) mColumn = 1 ;

        boolean noBefore = true ;
        for ( int i = 0 ; i < mBuffCurPos - 1 && noBefore ; ++i ) {
          if ( mReadBuffer.charAt( i ) != ' ' ) noBefore = false ;
        } // for

        if ( noBefore && mLine == 0 ) mLine = 1 ;

        mCurCh = GetChar();
        while ( !CheckEOF() && mCurCh != '\r' && mCurCh != '\n' ) {
          mCurCh = GetChar();
        } // while
      } // else if
      else {
        int tokenCol = mColumn;
        String val = String.valueOf( mCurCh ) ;
        mCurCh = GetChar();
        while ( !CheckEOF() && !IsSeparator( mCurCh ) ) {
          val += mCurCh;
          mCurCh = GetChar();
        } // while

        Token token = CreateToken( val, mLine, tokenCol );
        return token;
      } // else
    } // while

    return null;
  } // GetNextToken()

  public void AbortLine() throws Throwable {
    if ( GetNewLine() ) mCurCh = '\0' ;
  } // AbortLine()

  public void Reset() throws Throwable {
    boolean isAllwhiteSpace = true ;
    if ( mCurCh == '"' || mCurCh == '\'' || mCurCh == '(' || mCurCh == ')' ) {
      mLine = 1 ;
      mColumn = mLeftParenCount = 0 ;
      return ;
    } // if

    int i = -1 ;
    for ( i = mBuffCurPos ; i < mReadBuffer.length() && IsWhiteSpace( mReadBuffer.charAt( i ) ) ; ++i ) {
    } // for

    if ( i < mReadBuffer.length() && mReadBuffer.charAt( i ) != ';' ) isAllwhiteSpace = false ;

    if ( isAllwhiteSpace ) {
      if ( mBuffCurPos != 0 ) {
        GetNewLine() ;
      } // if

      mCurCh = '\0' ;
      mLine = 0;
    } // if
    else mLine = 1 ;

    mColumn = mLeftParenCount = 0 ;
  } // Reset()

  public Token CreateToken( String val, int line, int col ) {
    Token token = new Token();
    token.SetVal( val ).SetLine( line ).SetCol( col );
    return token;
  } // CreateToken()

  private boolean IsWhiteSpace( char c ) {
    return c == ' ' || c == '\t' || c == '\n' || c == '\r' ;
  } // IsWhiteSpace()

  private boolean IsSeparator( char c ) {
    return c == ' ' || c == '\n' || c == '\r' || c == '(' || c == ')' || c == '"' || c == '\'' ;
  } // IsSeparator()
} // class TokenParser

class SyntaxTree {
  public TreeNode mRoot;
  private TokenParser mInput ;

  public SyntaxTree() {
    mInput = TokenParser.GetInstance() ;
  } // SyntaxTree()

  public boolean CreateTree() throws Throwable {
    Token firstToken = mInput.GetNextToken();
    if ( firstToken != null ) {
      this.mRoot = Create( firstToken );
      return true;
    } // if

    return false;
  } // CreateTree()

  private TreeNode Create( Token token ) throws Throwable {
    boolean haveOneDot = false;
    if ( token == null )
      throw new EOFException() ;

    int dotIndex = -1 ;

    if ( token.TypeEquals( Type.LEFT_PAREN ) ) {
      Vector< TreeNode > treeNodes = new Vector< TreeNode >();
      Token nextToken = mInput.GetNextToken();

      if ( nextToken == null ) throw new EOFException() ;

      while ( !nextToken.TypeEquals( Type.RIGHT_PAREN ) ) {
        if ( dotIndex != -1 && treeNodes.size() - dotIndex == 2 ) {
          Token errorToken =
          mInput.CreateToken( String.valueOf( nextToken.GetVal().charAt( 0 ) ),
                              nextToken.GetLine(),
                              nextToken.GetCol() ) ;
          throw new UnexpectedCharacterException( errorToken ) ;
        } // if


        if ( nextToken.TypeEquals( Type.DOT ) ) {
          // Handle existed one dot in this pair before.
          if ( haveOneDot ) {
            if ( mInput.GetNowCharacter() == '\n' )
              throw new LineEnterEncountered( mInput.GetNowLine(), nextToken.GetCol() + 1 ) ;

            Token errorToken =
            mInput.CreateToken( String.valueOf( mInput.GetNowCharacter() ),
                                nextToken.GetLine(),
                                nextToken.GetCol() + 1 ) ;
            throw new UnexpectedCharacterException( errorToken ) ;
            // throw new UnexpectedCharacterException( nextToken ) ;
          } // if

          // handle single dot( no any left paren existed ) ---------------------
          if ( treeNodes.size() == 0 ) {
            if ( mInput.GetNowCharacter() == '\n' )
              throw new LineEnterEncountered( mInput.GetNowLine(), nextToken.GetCol() + 1 ) ;

            Token errorToken =
            mInput.CreateToken( String.valueOf( mInput.GetNowCharacter() ),
                                nextToken.GetLine(),
                                nextToken.GetCol() + 1 ) ;
            throw new UnexpectedCharacterException( errorToken ) ;
          } // if
          else {
            haveOneDot = true ;
            dotIndex = treeNodes.size() ;
          } // else
        } // if

        TreeNode node = Create( nextToken );
        treeNodes.add( node );
        nextToken = mInput.GetNextToken();
        if ( nextToken == null )
          throw new EOFException() ;
      } // while

      // Handle no any ATOM after dot, like this : ( 1 . )
      if ( dotIndex != -1 && treeNodes.size() - dotIndex == 1 )
        throw new UnexpectedCharacterException( nextToken ) ;

      // Create "Left Paren" TreeNode and its children.
      TreeNode root = ( new TreeNode( token, Type.LEFT_PAREN ) ).SetRight( CreateNIL() );
      TreeNode cur = root;
      TreeNode nowTreeNode;

      if ( treeNodes.size() == 0 ) {
        Token nilNode = new Token() ;
        nilNode.SetVal( "nil" ).SetLine( token.GetLine() ).SetCol( token.GetCol() ) ;
        root = new TreeNode( nilNode, Type.NIL ) ;
      } // if
      else {
        for ( int i = 0 ; i < treeNodes.size() ; ++i ) {
          nowTreeNode = treeNodes.get( i );
          if ( cur.GetLeft() == null ) {
            cur.SetLeft( nowTreeNode );
          } // if
          else {
            if ( nowTreeNode.TagEquals( Type.DOT ) ) {
              nowTreeNode = treeNodes.get( ++i );
              cur.SetRight( nowTreeNode );
              cur = cur.GetRight();
            } // if
            else {
              cur = cur.SetRight( CreateTempNode() ).GetRight();
              cur.SetLeft( nowTreeNode );
            } // else
          } // else
        } // for
      } // else

      return root;
    } // if
    else if ( token.TypeEquals( Type.QUOTE ) ) {
      Token tmp = new Token();
      tmp.SetVal( "quote" ).SetType( Type.QUOTE );
      tmp.SetLine( token.GetLine() ).SetCol( token.GetCol() );

      Token nextToken = mInput.GetNextToken();
      if ( nextToken == null )
        throw new EOFException() ;

      if ( nextToken.TypeEquals( Type.DOT ) ) {
        if ( mInput.GetNowCharacter() == '\n' )
          throw new LineEnterEncountered( mInput.GetNowLine(), mInput.GetNowColumn() );

        Token errorToken =
        mInput.CreateToken( String.valueOf( mInput.GetNowCharacter() ),
                            nextToken.GetLine(),
                            nextToken.GetCol() + 1 ) ;
        throw new UnexpectedCharacterException( errorToken ) ;
      } // if
      else if ( nextToken.TypeEquals( Type.RIGHT_PAREN ) ) {
        throw new UnexpectedCharacterException( nextToken );
      } // else if

      TreeNode right_Left = Create( nextToken );
      TreeNode root = CreateLeftParenNode();
      root.SetLeft( new TreeNode( tmp, Type.QUOTE ) );
      TreeNode right = root.SetRight( CreateTempNode() ).GetRight();
      right.SetLeft( right_Left );
      return root;
    } // else if
    else if ( token.TypeEquals( Type.DOT ) ) {
      return ( new TreeNode( token, Type.DOT ) ).SetLeft( CreateNIL() ).SetRight( CreateNIL() );
    } // else if
    else if ( token.TypeEquals( Type.RIGHT_PAREN ) ) {
      throw new UnexpectedCharacterException( token ) ;
    } // else if
    else {
      TreeNode node ;
      if ( token.TypeEquals( Type.NIL ) )
        node = ( new TreeNode( token, Type.NIL ) );
      else
        node = ( new TreeNode( token, Type.NORMAL ) );
      return node;
    } // else

  } // Create()

  private TreeNode CreateNIL() {
    Token tmp = new Token();
    tmp.SetVal( "nil" );
    tmp.SetType( Type.NIL ) ;
    TreeNode tmpTN = new TreeNode( tmp, Type.NIL ) ;
    return tmpTN ;
  } // CreateNIL()

  private TreeNode CreateLeftParenNode() {
    Token tmp = new Token();
    tmp.SetVal( "(" );
    TreeNode tmpTN = new TreeNode( tmp, Type.LEFT_PAREN ) ;
    tmpTN.SetLeft( CreateNIL() );
    tmpTN.SetRight( CreateNIL() );
    return tmpTN;
  } // CreateLeftParenNode()

  private TreeNode CreateTempNode() {
    Token tmp = new Token();
    TreeNode tmpTN = new TreeNode( tmp, Type.NOTHING ) ;
    tmpTN.SetLeft( CreateNIL() );
    tmpTN.SetRight( CreateNIL() );
    return tmpTN;
  } // CreateTempNode()

  public EvalResult Eval( Database db ) throws Throwable {
    EvalResult evr ;
    evr = Eval( mRoot, db ) ;
    return evr ;
  } // Eval()

  private EvalResult Eval( TreeNode root, Database db ) throws Throwable {
    if ( root == null ) return null ;
    else if ( root.GetVal().TypeEquals( Type.LEFT_PAREN ) ) {
      EvalResult leftResult = Eval( root.GetLeft(), db ) ;

      if ( leftResult.GetResult().GetVal().TypeEquals( Type.INT ) ||
           leftResult.GetResult().GetVal().TypeEquals( Type.FLOAT ) ||
           leftResult.GetResult().GetVal().TypeEquals( Type.T ) ||
           leftResult.GetResult().GetVal().TypeEquals( Type.NIL ) ||
           leftResult.GetResult().GetVal().TypeEquals( Type.STRING ) )
        throw new AttemptToApplyNonFunction( leftResult.GetResult() ) ;

      if ( !leftResult.IsClosure() &&
           !leftResult.IsFunction() &&
           ( leftResult.GetResult() != null &&
             leftResult.GetResult() != db.Get( leftResult.GetResult().GetVal().GetVal() ).GetResult() ) )
        throw new AttemptToApplyNonFunction( leftResult.GetResult() ) ;

      String command ;
      Closure closure ;
      if ( leftResult.IsClosure() ) {
        closure = leftResult.GetClosure() ;
        command = "closure" ;
        if ( root.GetLeft().GetVal().GetVal().equals( "(" ) )
          db.PushFunction( "lambda expression" );
        else
          db.PushFunction( root.GetLeft().GetVal().GetVal() );
      } // if
      else {
        closure = null ;
        command = leftResult.GetResult().GetVal().GetVal() ;
        db.PushFunction( root.GetLeft().GetVal().GetVal() );
      } // else

      EvalResult funcResult = null ;

      if ( command.equals( "cons" ) ) funcResult = HandleFunctionCons( root, db ) ;
      else if ( command.equals( "list" ) ) funcResult = HandleFunctionList( root, db ) ;
      else if ( command.equals( "quote" ) ) funcResult = HandleFunctionQuote( root, db ) ;
      else if ( command.equals( "exit" ) ) funcResult = HandleFunctionExit( root, db ) ;
      else if ( command.equals( "define" ) ) funcResult = HandleFunctionDefine( root, db ) ;
      else if ( command.equals( "clean-environment" ) ) funcResult = HandleFunctionCleanEnv( root, db ) ;
      else if ( command.equals( "car" ) ) funcResult = HandleFunctionCar( root, db ) ;
      else if ( command.equals( "cdr" ) ) funcResult = HandleFunctionCdr( root, db ) ;
      else if ( command.equals( "pair?" ) ) funcResult = HandleFunctionPair( root, db ) ;
      else if ( command.equals( "null?" ) ) funcResult = HandleFunctionNull( root, db ) ;
      else if ( command.equals( "integer?" ) ) funcResult = HanddleFunctionNumber( "integer?", root, db ) ;
      else if ( command.equals( "real?" ) ) funcResult = HanddleFunctionNumber( "real?", root, db ) ;
      else if ( command.equals( "number?" ) ) funcResult = HanddleFunctionNumber( "number?", root, db ) ;
      else if ( command.equals( "string?" ) ) funcResult = HandleFunctionString( root, db ) ;
      else if ( command.equals( "boolean?" ) ) funcResult = HandleFunctionBoolean( root, db ) ;
      else if ( command.equals( "symbol?" ) ) funcResult = HandleFunctionSymbol( root, db ) ;
      else if ( command.equals( "+" ) ) funcResult = HandleFunctionCalculate( "+", root, db ) ;
      else if ( command.equals( "-" ) ) funcResult = HandleFunctionCalculate( "-", root, db ) ;
      else if ( command.equals( "*" ) ) funcResult = HandleFunctionCalculate( "*", root, db ) ;
      else if ( command.equals( "/" ) ) funcResult = HandleFunctionCalculate( "/", root, db ) ;
      else if ( command.equals( ">" ) ) funcResult = HandleFunctionCompare( command, root, db ) ;
      else if ( command.equals( ">=" ) ) funcResult = HandleFunctionCompare( command, root, db ) ;
      else if ( command.equals( "<" ) ) funcResult = HandleFunctionCompare( command, root, db ) ;
      else if ( command.equals( "<=" ) ) funcResult = HandleFunctionCompare( command, root, db ) ;
      else if ( command.equals( "=" ) ) funcResult = HandleFunctionCompare( command, root, db ) ;
      else if ( command.equals( "not" ) ) funcResult = HandleFunctionNot( root, db ) ;
      else if ( command.equals( "and" ) ) funcResult = HandleFunctionAnd( root, db ) ;
      else if ( command.equals( "or" ) ) funcResult = HandleFunctionOr( root, db ) ;
      else if ( command.equals( "string-append" ) ) funcResult = HandleFunctionStringAppend( root, db ) ;
      else if ( command.equals( "string>?" ) ) funcResult = HandleFunctionStringCompare( root, db );
      else if ( command.equals( "eqv?" ) ) funcResult = HandleFunctionEqv( root, db );
      else if ( command.equals( "equal?" ) ) funcResult = HandleFunctionEqual( root, db );
      else if ( command.equals( "if" ) ) funcResult = HandleFunctionIf( root, db );
      else if ( command.equals( "cond" ) ) funcResult = HandleFunctionCond( root, db );
      else if ( command.equals( "begin" ) ) funcResult = HandleFunctionBegin( root, db );
      else if ( command.equals( "let" ) ) funcResult = HandleFunctionLet( root, db );
      else if ( command.equals( "lambda" ) ) funcResult = HandleFunctionLambda( root, db );
      else if ( command.equals( "closure" ) ) funcResult = HandleClosure( root, closure, db ) ;

      db.PopFunction() ;
      return funcResult ;
    } // if
    else if ( root.GetVal().TypeEquals( Type.SYMBOL ) || root.GetVal().TypeEquals( Type.QUOTE ) ) {
      if ( !db.Contain( root.GetVal().GetVal() ) )
        throw new UnboundSymbol( root.GetVal().GetVal() ) ;

      return db.Get( root.GetVal().GetVal() ) ;
    } // else if
    else return ( new EvalResult() ).SetResult( root );
  } // Eval()

  private EvalResult HandleFunctionCons( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    else if ( CountArguments( root.GetRight() ) != 2 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;
    else {
      EvalResult evr1, evr2 ;
      evr1 = EvalArgument( root.GetRight(), db ) ;
      evr2 = EvalArgument( root.GetRight().GetRight(), db ) ;

      TreeNode eRoot = CreateLeftParenNode() ;
      eRoot.SetLeft( evr1.GetResult() ).SetRight( evr2.GetResult() ) ;

      return new EvalResult( "cons", eRoot ) ;
    } // else
  } // HandleFunctionCons()

  private EvalResult HandleFunctionList( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    else if ( CountArguments( root.GetRight() ) == 0 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;
    else {
      Vector<EvalResult> results = new Vector< EvalResult >() ;
      for ( TreeNode node = root.GetRight() ;
            node != null && !node.TagEquals( Type.NIL ) ;
            node = node.GetRight() ) {
        EvalResult evr = EvalArgument( node, db ) ;
        results.add( evr ) ;
      } // for

      TreeNode eRoot = CreateLeftParenNode() ;
      TreeNode eWalk = eRoot ;

      for ( int i = 0 ; i < results.size() ; ++i ) {
        EvalResult evrE = results.get( i ) ;
        if ( eWalk == eRoot && eWalk.GetLeft().TagEquals( Type.NIL ) ) {
          eWalk.SetLeft( evrE.GetResult() ) ;
        } // if
        else {
          eWalk = eWalk.SetRight( CreateTempNode() ).GetRight() ;
          eWalk.SetLeft( evrE.GetResult() ) ;
        } // else
      } // for

      return new EvalResult( "list", eRoot ) ;
    } // else
  } // HandleFunctionList()

  public EvalResult HandleFunctionExit( TreeNode root, Database db ) throws Throwable {
    short check = 0 ;
    if ( root.TagEquals( Type.LEFT_PAREN ) ) check++ ;
    if ( root.GetLeft() != null && root.GetLeft().GetVal().GetVal().equals( "exit" ) ) check++ ;
    if ( root.GetRight() == null ) check++ ;
    if ( root.GetRight() != null && root.GetRight().TagEquals( Type.NIL ) ) check++ ;

    EvalResult evr = new EvalResult();
    if ( check >= 3 ) {
      if ( root == mRoot ) {
        System.out.println();
        evr.SetType( "exit" );
        return evr ;
      } // if
      else
        throw new LevelOfExit();
    } // if
    else {
      if ( !CheckPurePair( root ) ) throw new NonListException( root ) ;
      if ( CountArguments( root.GetRight() ) != 0 ) throw new IncorrectNumberOfArguments( "exit" ) ;
    } // else

    return null ;
  } // HandleFunctionExit()

  public EvalResult HandleFunctionDefine( TreeNode root, Database db ) throws Throwable {
    if ( root != mRoot ) throw new DefineFormatException();
    if ( !CheckPurePair( root ) ) throw new DefineFormatException();
    if ( CountArguments( root.GetRight() ) != 2 ) throw new DefineFormatException();

    TreeNode innRoot ;
    String symbolName ;
    if ( root.GetRight().GetLeft().TagEquals( Type.LEFT_PAREN ) ) {
      if ( CountArguments( root.GetRight().GetLeft() ) < 1 ) throw new DefineFormatException();
      symbolName = root.GetRight().GetLeft().GetLeft().GetVal().GetVal() ;
      if ( db.IsPrimtiveFunction( symbolName ) ) throw new DefineFormatException() ;
      if ( !root.GetRight().GetLeft().GetLeft().GetVal().TypeEquals( Type.SYMBOL ) )
        throw new DefineFormatException() ;

      innRoot = CreateLeftParenNode() ;
      innRoot.SetLeft( new TreeNode( ( new Token() ).SetVal( "lambda" ), Type.NORMAL ) ) ;
      TreeNode innPRoot = innRoot
      .SetRight( CreateTempNode() ).GetRight()
      .SetLeft( CreateLeftParenNode() ).GetLeft() ;

      boolean first = true ;
      for ( TreeNode w = root.GetRight().GetLeft().GetRight(), pr = innPRoot ;
            w != null && !w.TagEquals( Type.NIL ) ;
            w = w.GetRight() ) {
        if ( w.GetLeft().GetVal().TypeEquals( Type.SYMBOL ) ) {
          if ( first ) {
            pr.SetLeft( w.GetLeft() );
            first = false ;
          } // if
          else {
            pr = pr.SetRight( CreateTempNode() ).GetRight() ;
            pr.SetLeft( w.GetLeft() ) ;
          } // else
        } // if
        else throw new DefineFormatException() ;
      } // for

      TreeNode innERoot = innRoot.GetRight().SetRight( CreateTempNode() ).GetRight() ;
      if ( root.GetRight().GetRight().GetLeft().GetVal().TypeEquals( Type.SYMBOL ) )
        innERoot.SetLeft( db.Get( root.GetRight().GetRight().GetLeft().GetVal().GetVal() ).GetResult() ) ;
      else
        innERoot.SetLeft( root.GetRight().GetRight().GetLeft() ) ;
    } // if
    else {
      innRoot = root.GetRight().GetRight().GetLeft() ;
      symbolName = root.GetRight().GetLeft().GetVal().GetVal() ;
      if ( db.IsPrimtiveFunction( symbolName ) ) throw new DefineFormatException() ;
      if ( !root.GetRight().GetLeft().GetVal().TypeEquals( Type.SYMBOL ) )
        throw new DefineFormatException() ;
    } // else

    db.Add( symbolName, Eval( innRoot, db ) ) ;
    System.out.println( symbolName + " defined" ) ;

    return ( new EvalResult() ).SetType( "define" );
  } // HandleFunctionDefine()

  public EvalResult HandleFunctionCleanEnv( TreeNode root, Database db ) throws Throwable {
    if ( root != mRoot ) throw new CleanEnvNotRootException();
    if ( !CheckPurePair( root ) ) throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 0 )
      throw new IncorrectNumberOfArguments( "clean-environment" ) ;

    db.Clear();
    System.out.println( "environment cleaned" );
    return ( new EvalResult() ).SetType( "clean-env" );
  } // HandleFunctionCleanEnv()

  public EvalResult HandleFunctionQuote( TreeNode root, Database db ) throws Throwable {
    return new EvalResult( "quote", root.GetRight().GetLeft() ) ;
  } // HandleFunctionQuote()

  public EvalResult HandleFunctionCar( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) ) throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 ) throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult right = EvalArgument( root.GetRight(), db ) ;
    if ( !right.GetResult().GetVal().GetVal().equals( "(" ) )
      throw new IncorrectArgumentType( db.PopFunction(), right.GetResult() ) ;
    EvalResult result = new EvalResult( "car", right.GetResult().GetLeft() ) ;
    return result ;
  } // HandleFunctionCar()

  public EvalResult HandleFunctionCdr( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult right = EvalArgument( root.GetRight(), db ) ;

    if ( !CheckPair( right.GetResult() ) )
      throw new IncorrectArgumentType( db.PopFunction(), right.GetResult() ) ;

    TreeNode eRoot = CreateLeftParenNode() ;
    TreeNode eWalk = eRoot ;
    TreeNode walk ;
    for ( walk = right.GetResult().GetRight() ;
          walk != null && walk.GetLeft() != null ;
          walk = walk.GetRight() ) {
      if ( eWalk == eRoot && eWalk.GetLeft().TagEquals( Type.NIL ) )
        eWalk.SetLeft( walk.GetLeft() ) ;
      else {
        eWalk = eWalk.SetRight( CreateTempNode() ).GetRight() ;
        eWalk.SetLeft( walk.GetLeft() ) ;
      } // else
    } // for

    if ( walk.GetLeft() == null ) eWalk.SetRight( walk ) ;
    if ( eRoot.GetLeft().TagEquals( Type.NIL ) ) eRoot = walk ;
    return ( new EvalResult() ).SetType( "cdr" ).SetResult( eRoot );
  } // HandleFunctionCdr()

  public EvalResult HandleFunctionPair( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult right = EvalArgument( root.GetRight(), db );

    EvalResult evr ;
    TreeNode tmp ;
    if ( CheckPair( right.GetResult() ) ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionPair()

  public EvalResult HandleFunctionNull( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;
    EvalResult right = EvalArgument( root.GetRight(), db );

    // PrintTree( right.GetResult(), 0 , false, false, true ,db );

    EvalResult evr ;
    TreeNode tmp ;
    if ( right.GetResult().TagEquals( Type.NIL ) ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionNull()

  public EvalResult HanddleFunctionNumber( String command, TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult right = EvalArgument( root.GetRight(), db );
    EvalResult evr ;
    TreeNode tmp ;
    if ( command.equals( "integer?" ) ) {
      if ( Type.IsDigit( right.GetResult().GetVal().GetVal() ) ) {
        tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
        evr = new EvalResult( "true", tmp ) ;
      } // if
      else {
        tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
        evr = new EvalResult( "false", tmp ) ;
      } // else
    } // if
    else if ( command.equals( "real?" ) ) {
      if ( Type.IsFloat( right.GetResult().GetVal().GetVal() ) ) {
        tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
        evr = new EvalResult( "true", tmp ) ;
      } // if
      else {
        tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
        evr = new EvalResult( "false", tmp ) ;
      } // else
    } // else if
    else {
      if ( Type.IsDigit( right.GetResult().GetVal().GetVal() ) ||
           Type.IsFloat( right.GetResult().GetVal().GetVal() ) ) {
        tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
        evr = new EvalResult( "true", tmp ) ;
      } // if
      else {
        tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
        evr = new EvalResult( "false", tmp ) ;
      } // else
    } // else if

    return evr ;
  } // HanddleFunctionNumber()

  public EvalResult HandleFunctionString( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;
    EvalResult right = EvalArgument( root.GetRight(), db ) ;
    EvalResult evr ;
    TreeNode tmp ;
    if ( right.GetResult().GetVal().TypeEquals( Type.STRING ) ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionString()

  public EvalResult HandleFunctionBoolean( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult right = EvalArgument( root.GetRight(), db );
    EvalResult evr ;
    TreeNode tmp ;
    if ( right.GetResult().GetVal().TypeEquals( Type.T ) ||
         right.GetResult().GetVal().TypeEquals( Type.NIL ) ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionBoolean()

  public EvalResult HandleFunctionSymbol( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult right = EvalArgument( root.GetRight(), db );
    EvalResult evr ;
    TreeNode tmp ;
    if ( right.GetResult().GetVal().TypeEquals( Type.SYMBOL ) ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionSymbol()

  public EvalResult HandleFunctionCalculate( String command, TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) < 2 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    boolean paramNoFloat = true ;
    Vector<EvalResult> results = new Vector< EvalResult >() ;
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {
      EvalResult evr = EvalArgument( node, db ) ;
      if ( !evr.GetResult().GetVal().TypeEquals( Type.INT ) &&
           !evr.GetResult().GetVal().TypeEquals( Type.FLOAT ) )
        throw new IncorrectArgumentType( command, evr.GetResult() ) ;
      if ( evr.GetResult().GetVal().TypeEquals( Type.FLOAT ) ) paramNoFloat = false ;
      results.add( evr ) ;
    } // for

    EvalResult result = null ;
    int index = 0 ;
    float answer = Float.parseFloat( results.get( index++ ).GetResult().GetVal().GetVal() ) ;
    while ( index < results.size() ) {
      if ( command.equals( "+" ) )
        answer += Float.parseFloat( results.get( index++ ).GetResult().GetVal().GetVal() ) ;
      else if ( command.equals( "-" ) )
        answer -= Float.parseFloat( results.get( index++ ).GetResult().GetVal().GetVal() ) ;
      else if ( command.equals( "*" ) )
        answer *= Float.parseFloat( results.get( index++ ).GetResult().GetVal().GetVal() ) ;
      else {
        if ( Float.parseFloat( results.get( index ).GetResult().GetVal().GetVal() ) == 0.0 ) ;
        else answer /= Float.parseFloat( results.get( index++ ).GetResult().GetVal().GetVal() ) ;
      } // else

      if ( paramNoFloat ) {
        TreeNode tmp = new TreeNode( ( new Token() ).SetVal( Integer.toString( ( int ) answer ) ),
                                       Type.NORMAL ) ;
        result =  ( new EvalResult() ).SetType( command ).SetResult( tmp ) ;
      } // if
      else {
        TreeNode tmp = new TreeNode( ( new Token() ).SetVal( Float.toString( answer ) ),
                                       Type.NORMAL ) ;
        result = ( new EvalResult() ).SetType( command ).SetResult( tmp ) ;
      } // else
    } // while

    return result ;
  } // HandleFunctionCalculate()

  public EvalResult HandleFunctionNot( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult right = EvalArgument( root.GetRight(), db ) ;
    EvalResult evr ;
    TreeNode tmp ;
    if ( right.GetResult().GetVal().TypeEquals( Type.NIL ) ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionNot()

  public EvalResult HandleFunctionAnd( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) < 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    boolean evalResult = true ;
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) && evalResult ;
          node = node.GetRight() ) {
      EvalResult evr = EvalArgument( node, db ) ;
      if ( evr.GetResult().GetVal().TypeEquals( Type.NIL ) )
        evalResult = false ;
    } // for

    if ( evalResult )
      return new EvalResult( "true", new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ) ;
    else
      return new EvalResult( "false", new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ) ;
  } // HandleFunctionAnd()

  public EvalResult HandleFunctionOr( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) < 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    boolean evalResult = false ;
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) && !evalResult ;
          node = node.GetRight() ) {
      EvalResult evr = EvalArgument( node, db ) ;
      if ( !evr.GetResult().GetVal().TypeEquals( Type.NIL ) )
        evalResult = true ;
    } // for

    if ( evalResult )
      return new EvalResult( "true", new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ) ;
    else
      return new EvalResult( "false", new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ) ;
  } // HandleFunctionOr()

  public EvalResult HandleFunctionCompare( String command, TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) < 2 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    Vector<EvalResult> results = new Vector< EvalResult >() ;
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {
      EvalResult evr = EvalArgument( node, db );
      if ( evr.GetResult().GetVal().TypeEquals( Type.INT ) ||
           evr.GetResult().GetVal().TypeEquals( Type.FLOAT ) )
        results.add( evr ) ;
      else
        throw new IncorrectArgumentType( command, evr.GetResult() ) ;
    } // for

    boolean result = true ;
    for ( int i = 0 ; i < results.size() - 1 ; ++i ) {
      if ( command.equals( ">" ) ) {
        boolean thisResult = Float.parseFloat( results.get( i ).GetResult().GetVal().GetVal() ) >
                             Float.parseFloat( results.get( i+1 ).GetResult().GetVal().GetVal() ) ;
        result = result && thisResult ;
      } // if
      else if ( command.equals( ">=" ) ) {
        boolean thisResult = Float.parseFloat( results.get( i ).GetResult().GetVal().GetVal() ) >=
                Float.parseFloat( results.get( i+1 ).GetResult().GetVal().GetVal() ) ;
        result = result && thisResult ;
      } // if
      else if ( command.equals( "<" ) ) {
        boolean thisResult = Float.parseFloat( results.get( i ).GetResult().GetVal().GetVal() ) <
                Float.parseFloat( results.get( i+1 ).GetResult().GetVal().GetVal() ) ;
        result = result && thisResult ;
      } // if
      else if ( command.equals( "<=" ) ) {
        boolean thisResult = Float.parseFloat( results.get( i ).GetResult().GetVal().GetVal() ) <=
                Float.parseFloat( results.get( i+1 ).GetResult().GetVal().GetVal() ) ;
        result = result && thisResult ;
      } // if
      else if ( command.equals( "=" ) ) {
        boolean thisResult = Float.parseFloat( results.get( i ).GetResult().GetVal().GetVal() ) ==
                Float.parseFloat( results.get( i+1 ).GetResult().GetVal().GetVal() ) ;
        result = result && thisResult ;
      } // if
    } // for

    EvalResult evr ;
    TreeNode tmp ;
    if ( result ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.T ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionCompare()

  public EvalResult HandleFunctionStringAppend( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) < 2 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    String result = new String() ;
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {
      EvalResult evr = EvalArgument( node, db );
      if ( evr.GetResult().GetVal().TypeEquals( Type.STRING ) ) {
        String s = evr.GetResult().GetVal().GetVal() ;
        result += s.substring( 1, s.length() - 1 ) ;
      } // if
      else
        throw new IncorrectArgumentType( "string-append", evr.GetResult() ) ;
    } // for

    result = "\"" + result + "\"" ;
    EvalResult evr ;
    TreeNode tmp ;
    tmp = new TreeNode( ( new Token() ).SetVal( result ), Type.NORMAL ) ;
    evr = new EvalResult( "string", tmp ) ;

    return evr ;
  } // HandleFunctionStringAppend()

  public EvalResult HandleFunctionStringCompare( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) < 2 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    boolean result = true ;
    String curStr = new String() ;

    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {
      EvalResult evr = EvalArgument( node, db ) ;
      if ( evr.GetResult().GetVal().TypeEquals( Type.STRING ) ) {
        String str = evr.GetResult().GetVal().GetVal() ;
        boolean thisResult = true ;
        if ( !curStr.isEmpty() ) thisResult = curStr.compareTo( str ) > 0 ;
        result = result && thisResult ;
        curStr = str ;
      } // if
      else throw new IncorrectArgumentType( "string>?", evr.GetResult() ) ;
    } // for

    EvalResult evr ;
    TreeNode tmp ;
    if ( result ) {
      tmp = new TreeNode( ( new Token() ).SetVal( "#t" ), Type.NORMAL ) ;
      evr = new EvalResult( "true", tmp ) ;
    } // if
    else {
      tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionStringCompare()

  public EvalResult HandleFunctionEqv( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 2 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult evr1 = EvalArgument( root.GetRight(), db ) ;
    EvalResult evr2 = EvalArgument( root.GetRight().GetRight(), db ) ;
    String s1 = root.GetRight().GetLeft().GetVal().GetVal() ;
    String s2 = root.GetRight().GetRight().GetLeft().GetVal().GetVal() ;

    TreeNode r1 = evr1.GetResult(), r2 = evr2.GetResult() ;
    boolean r1Atom = r1.GetVal().TypeEquals( Type.INT ) || r1.GetVal().TypeEquals( Type.FLOAT ) ||
                     r1.GetVal().TypeEquals( Type.T ) || r1.GetVal().TypeEquals( Type.NIL ) ;
    boolean r2Atom = r2.GetVal().TypeEquals( Type.INT ) || r2.GetVal().TypeEquals( Type.FLOAT ) ||
                     r2.GetVal().TypeEquals( Type.T ) || r2.GetVal().TypeEquals( Type.NIL ) ;
    EvalResult evr ;
    if ( r1Atom && r2Atom ) {
      boolean equ = r1.GetVal().GetVal().equals( r2.GetVal().GetVal() ) ;
      TreeNode tmp = new TreeNode( ( new Token() ).SetVal( equ ? "#t" : "nil" ),
                                   equ ? Type.T : Type.NIL ) ;
      evr = new EvalResult( ( equ ? "true" : "false" ), tmp ) ;
    } // if
    else if ( !r1Atom && !r2Atom ) {
      boolean equ = false ;
      if ( db.Contain( s1 ) && db.Contain( s2 ) ) equ = db.Get( s1 ) == db.Get( s2 ) ;

      TreeNode tmp = new TreeNode( ( new Token() ).SetVal( equ ? "#t" : "nil" ),
                                   equ ? Type.T : Type.NIL ) ;
      evr = new EvalResult( ( equ ? "true" : "false" ), tmp ) ;
    } // else if
    else {
      TreeNode tmp = new TreeNode( ( new Token() ).SetVal( "nil" ), Type.NIL ) ;
      evr = new EvalResult( "false", tmp ) ;
    } // else

    return evr ;
  } // HandleFunctionEqv()

  public EvalResult HandleFunctionEqual( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 2 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult evr1 = EvalArgument( root.GetRight(), db ) ;
    EvalResult evr2 = EvalArgument( root.GetRight().GetRight(), db ) ;
    String s1 = root.GetRight().GetLeft().GetVal().GetVal() ;
    String s2 = root.GetRight().GetRight().GetLeft().GetVal().GetVal() ;

    TreeNode r1 = evr1.GetResult(), r2 = evr2.GetResult() ;
    boolean equ = EqualTree( r1, r2 ) ;
    TreeNode tmp = new TreeNode( ( new Token() ).SetVal( equ ? "#t" : "nil" ), Type.NIL ) ;
    return new EvalResult( ( equ ? "true" : "false" ), tmp ) ;
  } // HandleFunctionEqual()

  public EvalResult HandleFunctionIf( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) != 3 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    EvalResult condition = EvalArgument( root.GetRight(), db ) ;
    EvalResult result ;
    if ( ! condition.GetResult().GetVal().TypeEquals( Type.NIL ) ) {
      result = EvalArgument( root.GetRight().GetRight(), db ) ;
    } // if
    else {
      result = EvalArgument( root.GetRight().GetRight().GetRight(), db ) ;
    } // else

    return result ;
  } // HandleFunctionIf()

  public EvalResult HandleFunctionCond( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) < 1 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    // Check parameter are legal
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {
      if ( CountArguments( node.GetLeft() ) < 2 || !CheckPurePair( node.GetLeft() ) )
        throw new ParameterformatException( node.GetLeft() ) ;
    } // for

    EvalResult result = new EvalResult() ;
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {

      EvalResult condition = EvalArgument( node.GetLeft(), db ) ;
      /*
      TreeNode condiRoot = node.GetLeft() ;
      if ( condiRoot.GetLeft().GetVal().TypeEquals( Type.SYMBOL ) ) {
        if ( db.Contain( condiRoot.GetLeft().GetVal().GetVal() ) )
          condition = Eval( db.Get( condiRoot.GetLeft().GetVal().GetVal() ), db );
        else throw new UnboundSymbol( condiRoot.GetLeft().GetVal().GetVal() );
      } // if
      else condition = Eval( condiRoot.GetLeft(), db ) ;
      */

      if ( !condition.GetResult().TagEquals( Type.NIL ) ) {
        for ( TreeNode iNode = node.GetLeft().GetRight() ;
              iNode != null && !iNode.TagEquals( Type.NIL ) ;
              iNode = iNode.GetRight() ) {
          TreeNode right = iNode.GetLeft() ;
          result = EvalArgument( iNode, db ) ;
          /*
          if ( right.GetVal().TypeEquals( Type.SYMBOL ) ) {
            if ( db.Contain( right.GetVal().GetVal() ) )
              result = Eval( db.Get( right.GetVal().GetVal() ), db ) ;
            else
              throw new UnboundSymbol( right.GetVal().GetVal() ) ;
          } // if
          else result = Eval( right, db ) ;
          */
        } // for

        return result ;
      } // if
    } // for

    throw new NoReturnValue( "cond" ) ;
  } // HandleFunctionCond()

  public EvalResult HandleFunctionBegin( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) )
      throw new NonListException( root ) ;
    if ( CountArguments( root.GetRight() ) == 0 )
      throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    Vector<EvalResult> results = new Vector< EvalResult >() ;
    for ( TreeNode node = root.GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {
      results.add( EvalArgument( node, db ) ) ;
    } // for

    return results.lastElement() ;
  } // HandleFunctionBegin()

  public EvalResult HandleFunctionLet( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) || CountArguments( root.GetRight() ) < 2 )
      throw new LetFormatException() ;

    TreeNode argsRoot = root.GetRight().GetLeft() ;
    if ( !argsRoot.TagEquals( Type.LEFT_PAREN ) && !argsRoot.TagEquals( Type.NIL ) )
      throw new LetFormatException() ;

    for ( TreeNode t = argsRoot ; t != null && !t.TagEquals( Type.NIL ) ; t = t.GetRight() ) {
      TreeNode innRoot = t.GetLeft() ;
      if ( !innRoot.TagEquals( Type.LEFT_PAREN ) ||
           !innRoot.GetLeft().GetVal().TypeEquals( Type.SYMBOL ) )
        throw new LetFormatException() ;
    } // for

    Database innDb = new Database( db ) ;
    if ( argsRoot.TagEquals( Type.LEFT_PAREN ) ) {
      // Define local variables
      for ( TreeNode t = argsRoot ; t != null && !t.TagEquals( Type.NIL ) ; t = t.GetRight() ) {
        if ( CountArguments( t.GetLeft() ) != 2 ) throw new LetFormatException() ;
        TreeNode innRoot = t.GetLeft() ;
        if ( !innRoot.TagEquals( Type.LEFT_PAREN ) ) throw new LetFormatException() ;
        if ( innRoot.GetLeft().GetVal().TypeEquals( Type.SYMBOL ) ) {
          String symbolName = innRoot.GetLeft().GetVal().GetVal() ;
          if ( innRoot.GetRight().GetLeft().GetVal().TypeEquals( Type.SYMBOL ) )
            innDb.Add( symbolName, db.Get( innRoot.GetRight().GetLeft().GetVal().GetVal() ) );
          else
            innDb.Add( symbolName, Eval( innRoot.GetRight().GetLeft(), db ) ) ;
        } // if
        else throw new LetFormatException() ;
      } // for
    } // if

    Vector<EvalResult> results = new Vector< EvalResult >() ;
    for ( TreeNode node = root.GetRight().GetRight() ;
          node != null && !node.TagEquals( Type.NIL ) ;
          node = node.GetRight() ) {
      results.add( EvalArgument( node, innDb ) ) ;
    } // for

    return results.lastElement() ;
  } // HandleFunctionLet()

  public EvalResult HandleFunctionLambda( TreeNode root, Database db ) throws Throwable {
    if ( !CheckPurePair( root ) || CountArguments( root.GetRight() ) < 2 )
      throw new LambdaFormatException() ;

    Vector<String> params = new Vector< String >() ;
    if ( !root.GetRight().GetLeft().TagEquals( Type.LEFT_PAREN ) &&
         !root.GetRight().GetLeft().TagEquals( Type.NIL ) )
      throw new LambdaFormatException() ;

    for ( TreeNode w = root.GetRight().GetLeft() ;
          w != null && !w.TagEquals( Type.NIL ) ;
          w = w.GetRight() ) {
      if ( w.GetLeft().GetVal().TypeEquals( Type.SYMBOL ) )
        params.add( w.GetLeft().GetVal().GetVal() ) ;
      else throw new LambdaFormatException();
    } // for

    Vector<TreeNode> excuts = new Vector< TreeNode >() ;
    for ( TreeNode w = root.GetRight().GetRight() ;
          w != null && !w.TagEquals( Type.NIL ) ;
          w = w.GetRight() ) {
      excuts.add( w.GetLeft() ) ;
    } // for

    // db.AddUserFunction( root ) ;
    Closure c = new Closure( params, excuts ) ;
    TreeNode tmp = new TreeNode( ( new Token() ).SetVal( "#<user function>" ), Type.NORMAL ) ;
    EvalResult e = ( new EvalResult( c, false ) )
    .SetType( "lambda" )
    .SetResult( tmp ) ;
    return e ;
  } // HandleFunctionLambda()

  public EvalResult HandleClosure( TreeNode root, Closure c, Database db ) throws Throwable {
    Vector<String> params = c.GetParams() ;
    Vector<TreeNode> excuts = c.GetExcutions() ;
    Database innDb = new Database( db ) ;

    // let the variavle
    int index = 0 ;
    for ( TreeNode w = root.GetRight() ;
          w != null && !w.TagEquals( Type.NIL ) ;
          w = w.GetRight(), ++index ) {
      if ( params.isEmpty() || params.size() == index )
        throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

      if ( w.GetLeft().GetVal().TypeEquals( Type.SYMBOL ) ) {
        EvalResult t = db.Get( w.GetLeft().GetVal().GetVal() ) ;
        if ( t == null ) throw new UnboundSymbol( w.GetLeft().GetVal().GetVal() ) ;
        innDb.Add( params.get( index ), db.Get( w.GetLeft().GetVal().GetVal() ) ) ;
      } // if
      else {
        TreeNode t = w.GetLeft() ;
        EvalResult result = Eval( t, db ) ;
        innDb.Add( params.get( index ), result );
      } // else
    } // for

    if ( index != params.size() ) throw new IncorrectNumberOfArguments( db.PopFunction() ) ;

    // System.out.println( "開始執行參數！" );
    Vector<EvalResult> results = new Vector< EvalResult >() ;
    // System.out.println( "參數為：" );
    for ( int i = 0 ; i < excuts.size() ; ++i ) {
      // PrintTree( excuts.get( i ), 0, false, false, true, db ) ;
      EvalResult evr ;
      String s1 = excuts.get( i ).GetVal().GetVal() ;

      if ( excuts.get( i ).GetVal().TypeEquals( Type.SYMBOL ) ) {
        if ( innDb.Contain( s1 ) ) evr = innDb.Get( s1 ) ;
        else throw new UnboundSymbol( excuts.get( i ).GetVal().GetVal() );
      } // if
      else evr = Eval( excuts.get( i ), innDb );

      results.add( evr ) ;
    } // for

    // System.out.println( "結束執行參數！且結論為：" );
    // PrintTree( results.lastElement().GetResult(), 0, false, false, true, innDb );
    // System.out.println( "\n");

    return results.lastElement() ;
  } // HandleClosure()

  private EvalResult EvalArgument( TreeNode node, Database db ) throws Throwable {
    EvalResult evr ;
    evr = Eval( node.GetLeft(), db ) ;
    return evr ;
  } // EvalArgument()

  private boolean EqualTree( TreeNode n1, TreeNode n2 ) {
    if ( n1 == null && n2 == null ) return true ;
    if ( ( n1 == null && n2 != null ) || ( n2 == null  && n1 != null ) )
      return false ;
    if ( n1 != null && n2 != null && !n1.GetVal().GetVal().equals( n2.GetVal().GetVal() ) )
      return false ;
    return EqualTree( n1.GetLeft(), n2.GetLeft() ) && EqualTree( n1.GetRight(), n2.GetRight() ) ;
  } // EqualTree()

  private boolean CheckPurePair( TreeNode node ) {
    for ( TreeNode walk = node.GetRight() ; walk != null ; walk = walk.GetRight() ) {
      if ( !walk.TagEquals( Type.LEFT_PAREN ) && !walk.TagEquals( Type.NOTHING ) &&
           !walk.TagEquals( Type.NIL ) )
        return false ;
    } // for

    return true ;
  } // CheckPurePair()

  private boolean CheckPair( TreeNode node ) {
    if ( node.TagEquals( Type.LEFT_PAREN ) ) {
      return true ;
    } // if

    return false ;
  } // CheckPair()

  private int CountArguments( TreeNode root ) {
    int i = 0 ;
    for ( TreeNode n = root ; n != null && !n.TagEquals( Type.NIL ) ; n = n.GetRight() ) {
      ++i ;
    } // for

    return i ;
  } // CountArguments()

  public void Travel( TreeNode node, Database db ) {
    PrintTree( node, 0, false, false, false, db );
  } // Travel()

  public void Travel( TreeNode node, boolean error, Database db ) {
    PrintTree( node, 0, false, false, error, db );
  } // Travel()

  private void PrintTree( TreeNode node, int depth, boolean isFirst, boolean frontParen, boolean error,
                          Database db ) {
    boolean first = true ;
    boolean paren = false ; // Remember this level print left paren.
    boolean dot = false ; // go right will be true.
    boolean isFirstTemp = isFirst ;

    for ( TreeNode walk = node ; walk != null ; walk = walk.GetRight() ) {
      if ( walk.TagEquals( Type.LEFT_PAREN ) ) {
        if ( !dot ) {
          PrintSpace( isFirstTemp && frontParen ? 0 : depth ) ;
          System.out.print( "( " ) ;
          paren = true ;
        } // if

        if ( first ) {
          if ( walk.GetLeft().TagEquals( Type.LEFT_PAREN ) ) {
            if ( paren ) PrintTree( walk.GetLeft(), depth + 2, true, paren, error, db );
            else PrintTree( walk.GetLeft(), depth, true, paren, error, db );
          } // if
          else {
            if ( paren ) PrintTree( walk.GetLeft(), depth + 2, true, paren, error, db ) ;
            else PrintTree( walk.GetLeft(), depth, true, paren, error, db );
          } // else

          depth += 2 ;
          first = false ;
          isFirstTemp = false ;
        } // if
        else {
          if ( walk.TagEquals( Type.LEFT_PAREN ) )
            PrintTree( walk.GetLeft(), depth, false, paren, error, db );
          else
            PrintTree( walk.GetLeft(), depth, false, paren, error, db ) ;
        } // else

        if ( walk.GetRight() != null &&
             !walk.GetRight().TagEquals( Type.NIL ) &&
             !walk.GetRight().TagEquals( Type.LEFT_PAREN ) &&
             !walk.GetRight().TagEquals( Type.NOTHING ) ) {
          PrintSpace( depth ) ;
          System.out.println( "." ) ;
        } // if

        dot = true ;
      } // if
      else if ( walk.TagEquals( Type.NOTHING ) ) {
        PrintTree( walk.GetLeft(), depth, true, false, error, db ) ;

        if ( walk.GetRight() != null &&
             !walk.GetRight().TagEquals( Type.NIL ) &&
             !walk.GetRight().TagEquals( Type.LEFT_PAREN ) &&
             !walk.GetRight().TagEquals( Type.NOTHING ) ) {
          PrintSpace( depth ) ;
          System.out.println( "." ) ;
        } // if

        dot = true ;
      } // else if
      else {
        if ( walk.GetVal().TypeEquals( Type.FLOAT ) ) {
          PrintSpace( isFirstTemp && frontParen ? 0 : depth );
          String s0 = walk.GetVal().GetVal() ;
          if ( s0.length() - s0.lastIndexOf( '.' ) > 4 )
            s0 = s0.substring( 0, s0.lastIndexOf( '.' ) + 4 ) ;
          System.out.printf( "%.3f\n", Float.parseFloat( s0 ) );
        } // if
        else if ( walk.GetVal().TypeEquals( Type.INT ) ) {
          PrintSpace( isFirstTemp && frontParen ? 0 : depth );
          if ( walk.GetVal().GetVal().charAt( 0 ) == '+' )
            System.out.printf( "%d\n", Integer.parseInt( walk.GetVal().GetVal().substring( 1 ) ) );
          else
            System.out.printf( "%d\n", Integer.parseInt( walk.GetVal().GetVal() ) );
        } // else if
        else if ( walk.GetVal().TypeEquals( Type.T ) ) {
          PrintSpace( isFirstTemp && frontParen ? 0 : depth ) ;
          System.out.println( "#t" );
        } // else if
        else if ( walk.TagEquals( Type.NIL ) ) {
          if ( !dot ) {
            PrintSpace( isFirstTemp && frontParen ? 0 : depth );
            System.out.println( "nil" );
          } // if
        } // else if
        else {
          PrintSpace( isFirstTemp && frontParen ? 0 : depth ) ;
          /*
          if ( !error ) {
            String symbolName = walk.GetVal().GetVal() ;
            if ( db.IsPrimtiveFunction( symbolName ) ) {
              if ( db.Get( symbolName ).GetResult() == walk )
                System.out.println( "#<internal function>" );
            } // if
            else
              System.out.println( walk.GetVal().GetVal() );
          } // if
          else
            System.out.println( walk.GetVal().GetVal() );
          */
          if ( !error &&
               db.IsPrimtiveFunction( walk.GetVal().GetVal() ) &&
               db.Get( walk.GetVal().GetVal() ).GetResult() == walk )
            System.out.println( "#<internal function>" );
          else
            System.out.println( walk.GetVal().GetVal() );


        } // else
      } // else
    } // for

    if ( paren ) {
      PrintSpace( depth - 2 ) ;
      System.out.println( ")" ) ;
    } // if
  } // PrintTree()

  private void PrintSpace( int number ) {
    for ( int i = 0 ; i < number ; ++i )
      System.out.print( " " );
  } // PrintSpace()
} // class SyntaxTree

class Database {
  private final static String PS[] = { "define", "exit", "cons", "list", "quote", "car", "cdr", "pair?",
                                       "null?", "integer?", "real?", "number?", "string?", "boolean?",
                                       "symbol?", "+", "-", "*", "/", "not", "and", "or", ">", ">=", "<",
                                       "<=", "=", "string-append", "string>?", "eqv?", "equal?", "begin",
                                       "if", "cond", "clean-environment", "let", "lambda"
                                     };
  private LinkedHashMap<String, EvalResult> mBindingMap ;
  private Database mFather ;
  private Stack<String> mCurFunction ;

  public Database() {
    mBindingMap = new LinkedHashMap< String, EvalResult >() ;
    mCurFunction = new Stack<String>() ;
  } // Database()

  public Database( Database db ) {
    mBindingMap = new LinkedHashMap< String, EvalResult >() ;
    mFather = db ;
    mCurFunction = mFather.GetCurFunction() ;
  } // Database()

  public void AddPrimtiveFunction() {
    for ( int i = 0 ; i < PS.length ; ++i ) {
      EvalResult e = new EvalResult() ;
      e.SetResult( new TreeNode( ( new Token() ).SetVal( PS[ i ] ), Type.NORMAL ) ) ;
      e.SetFunctionBool( true ) ;
      e.SetType( "function" ) ;
      Add( PS[ i ], e );
    } // for
  } // AddPrimtiveFunction()

  public void Add( String str, EvalResult node ) {
    mBindingMap.put( str, node );
  } // Add()

  public EvalResult Get( String str ) {
    if ( mBindingMap.containsKey( str ) ) {
      return mBindingMap.get( str );
    } // if
    else if ( mFather != null && mFather.Contain( str ) )
      return mFather.Get( str ) ;
    else
      return null ;
  } // Get()

  public void Clear() {
    mBindingMap.clear();
    AddPrimtiveFunction() ;
  } // Clear()

  public boolean Contain( String str ) {
    return ( mBindingMap.containsKey( str ) || ( mFather != null && mFather.Contain( str ) ) ) ;
  } // Contain()

  public static boolean IsPrimtiveFunction( String str ) {
    for ( int i = 0 ; i < PS.length ; ++i ) {
      if ( PS[i].equals( str ) ) return true ;
    } // for

    return false ;
  } // IsPrimtiveFunction()

  public Stack<String> GetCurFunction() {
    return this.mCurFunction ;
  } // GetCurFunction()

  public void PushFunction( String s ) {
    mCurFunction.push( s ) ;
  } // PushFunction()

  public String PopFunction() {
    return mCurFunction.pop() ;
  } // PopFunction()

  public void ClearFunction() {
    mCurFunction = new Stack< String >() ;
  } // ClearFunction()
} // class Database

class TreeNode {
  private Token mVal;
  private String mTag;
  private TreeNode mLeft, mRight ;

  public TreeNode( Token t, String tag ) {
    this.mVal = t;
    this.mTag = tag;
    this.mLeft = this.mRight = null ;
  } // TreeNode()

  public TreeNode SetLeft( TreeNode node ) {
    this.mLeft = node;
    return this;
  } // SetLeft()

  public TreeNode SetRight( TreeNode node ) {
    this.mRight = node;
    return this;
  } // SetRight()

  public TreeNode SetType( String str ) {
    this.mTag = str ;
    return this;
  } // SetType()

  public Token GetVal() {
    return this.mVal;
  } // GetVal()

  public TreeNode GetLeft() {
    return this.mLeft;
  } // GetLeft()

  public TreeNode GetRight() {
    return this.mRight;
  } // GetRight()

  public String GetType() {
    return this.mTag ;
  } // GetType()

  public boolean TagEquals( String tag ) {
    return mTag.equals( tag ) ;
  } // TagEquals();
} // class TreeNode

class Token {
  private String mVal;
  private String mType;
  private int mLine = 0, mCol = 0;

  public Token() {
    mVal = "" ;
    mType = Type.UNKNOWN ;
  } // Token()

  public Token SetVal( String val ) {
    if ( val == null || val.isEmpty() ) return this ;
    this.mVal = val;
    SetType_( val );
    if ( this.mType.equals( Type.FLOAT ) ) {
      if ( mVal.length() - mVal.lastIndexOf( '.' ) > 4 )
        mVal = mVal.substring( 0, mVal.lastIndexOf( '.' ) + 4 ) ;
    } // if

    return this;
  } // SetVal()

  private void SetType_( String val ) {
    this.mType = Type.SearchType( val );
  } // SetType_()

  public Token SetType( String val ) {
    this.mType = val ;
    return this;
  } // SetType()

  public Token SetLine( int line ) {
    this.mLine = line;
    return this;
  } // SetLine()

  public Token SetCol( int col ) {
    this.mCol = col;
    return this;
  } // SetCol()

  public String GetVal() {
    return this.mVal;
  } // GetVal()

  public int GetLine() {
    return this.mLine;
  } // GetLine()

  public int GetCol() {
    return this.mCol;
  } // GetCol()

  public String GetType() { return this.mType; } // GetType()

  public boolean TypeEquals( String str ) {
    return mType.equals( str ) ;
  } // TypeEquals()
} // class Token

final class Type {
  public final static String UNKNOWN = "UNKNOWN" ;
  public final static String LEFT_PAREN = "LEFT_PAREN" ;
  public final static String RIGHT_PAREN = "RIGHT_PAREN" ;
  public final static String INT = "INT" ;
  public final static String STRING = "STRING" ;
  public final static String DOT = "DOT" ;
  public final static String FLOAT = "FLOAT" ;
  public final static String NIL = "NIL" ;
  public final static String T = "T" ;
  public final static String QUOTE = "QUOTE" ;
  public final static String COMMENT = "COMMENT" ;
  public final static String SYMBOL = "SYMBOL" ;
  public final static String NORMAL = "NORMAL" ;
  public final static String NOTHING = "NOTHING" ;
  public final static String FUNCTION = "FUNCTION" ;

  public static String SearchType( String str ) {
    if ( str.equals( ")" ) ) return RIGHT_PAREN;
    else if ( str.equals( "(" ) ) return LEFT_PAREN;
    else if ( str.equals( "." ) ) return DOT;
    else if ( str.equals( "#f" ) || str.equals( "nil" ) ) return NIL;
    else if ( str.equals( "#t" ) || str.equals( "t" ) ) return T;
    else if ( str.equals( "'" ) ) return QUOTE;
    else if ( str.charAt( 0 ) == '"' && ( str.length() > 0 && str.charAt( str.length() - 1 ) == '"' ) )
      return STRING;
    else if ( IsFloat( str ) ) return FLOAT;
    else if ( IsDigit( str ) ) return INT;
    else if ( IsCommit( str ) ) return COMMENT;
    else return SYMBOL;
  } // SearchType()

  public static boolean IsFloat( String str ) {
    short dotCount = 0, numberCount = 0;
    char fc = str.charAt( 0 );
    if ( fc == '+' || fc == '-' || fc == '.' || ( fc >= '0' && fc <= '9' ) ) {
      if ( fc >= '0' && fc <= '9' ) numberCount++;
      else if ( fc == '.' ) dotCount++;

      boolean breaking = false;
      for ( int i = 1; i < str.length() && !breaking ; ++i ) {
        char c = str.charAt( i );
        if ( ( c < '0' || c > '9' ) && c != '.' ) breaking = true;
        if ( !breaking && c == '.' ) dotCount++;
        if ( c >= '0' && c <= '9' ) numberCount++;
      } // for

      if ( dotCount != 1 || numberCount == 0 || breaking ) return false;
      return true;
    } // if

    return false;
  } // IsFloat()

  public static boolean IsDigit( String str ) {
    short numberCount = 0 ;
    char fc = str.charAt( 0 ) ;
    if ( fc == '+' || fc == '-' || ( fc >= '0' && fc <= '9' ) ) {
      if ( fc >= '0' && fc <= '9' ) numberCount++ ;

      for ( int i = 1; i < str.length() ; ++i ) {
        char c = str.charAt( i );
        if ( c >= '0' && c <= '9' ) numberCount++ ;
        if ( c < '0' || c > '9' ) return false;
      } // for

      return numberCount != 0;
    } // if

    return false ;
  } // IsDigit()

  private static boolean IsCommit( String str ) {
    return str.charAt( 0 ) == ';' ;
  } // IsCommit()
} // class Type

class EvalResult {
  private String mType ;
  private TreeNode mResult ;

  private boolean mIsFunction = false ;
  private boolean mIsClosure = false ;

  private Closure mClosure ;

  public EvalResult() {
    mType = new String() ;
    mIsClosure = false ;
    mIsFunction = false ;
  } // EvalResult()

  public EvalResult( Closure closure, boolean isFunction ) {
    mType = "closure" ;
    mIsFunction = isFunction ;
    mIsClosure = !isFunction ;
    mClosure = closure ;
    mResult = new TreeNode( ( new Token() ).SetVal( "#<user function>" ), Type.FUNCTION ) ;
  } // EvalResult()

  public EvalResult( String type, TreeNode result ) {
    mType = type ;
    mResult = result ;
    mIsClosure = false ;
    mIsFunction = false ;
  } // EvalResult()

  private void SetType() {
    mType = mResult.GetVal().GetType() ;
  } // SetType()

  public EvalResult SetType( String s ) {
    mType = s ;
    return this ;
  } // SetType()

  public EvalResult SetResult( TreeNode node ) {
    if ( node != null ) this.mResult = node ;
    SetType();
    return this;
  } // SetResult()

  public EvalResult SetFunctionBool( boolean isFunction ) {
    mIsFunction = isFunction ;
    return this ;
  } // SetFunctionBool()

  public TreeNode GetResult() {
    return this.mResult ;
  } // GetResult()

  public String GetType() { return this.mType ; } // GetType()

  public boolean TagEquals( String s ) { return mType.equals( s ); } // TagEquals()

  public boolean IsClosure() {
    return mIsClosure;
  } // IsClosure()

  public Closure GetClosure() {
    return mClosure ;
  } // GetClosure()

  public boolean IsFunction() {
    return mIsFunction;
  } // IsFunction()
} // class EvalResult

// handle lambda expression
class Closure {
  private Vector<String> mParams ;
  private Vector<TreeNode> mExcutes ;

  public Closure( Vector<String> p, Vector<TreeNode> e ) {
    this.mParams = p ;
    this.mExcutes = e ;
  } // Closure()

  public Vector<String> GetParams() { return this.mParams ; } // GetParams()

  public Vector<TreeNode> GetExcutions() { return  this.mExcutes ; } // GetExcutions()
} // class Closure

class UnexpectedCharacterException extends Exception {
  private Token mToken ;
  public UnexpectedCharacterException( Token token ) {
    this.mToken = token ;
  } // UnexpectedCharacterException()

  public Token GetToken() {
    return this.mToken ;
  } // GetToken();
} // class UnexpectedCharacterException

class LineEnterEncountered extends Exception {
  private int mLine, mCol ;
  public LineEnterEncountered( int line, int col ) {
    this.mLine = line ;
    this.mCol = col ;
  } // LineEnterEncountered()

  public int GetLine() {
    return this.mLine ;
  } // GetLine()

  public int GetColumn() {
    return this.mCol ;
  } // GetColumn()
} // class LineEnterEncountered

class EOFException extends Exception {
} // class EOFException

class IncorrectNumberOfArguments extends Exception {
  private String mErrCommand ;
  public IncorrectNumberOfArguments( String s ) { mErrCommand = s ; } // IncorrectNumberOfArguments()

  public String GetErrCommandName() { return mErrCommand ; } // GetErrCommandName()
} // class IncorrectNumberOfArguments

class NonListException extends Exception {
  private TreeNode mErrNode ;
  public NonListException( TreeNode node ) { mErrNode = node ; } // NonListException()

  public TreeNode GetErrNdoe() { return mErrNode ; } // GetErrCommandName()
} // class NonListException

class UnboundSymbol extends Exception {
  private String mErrSymbolName ;
  public UnboundSymbol( String node ) { mErrSymbolName = node ; } // UnboundSymbol()

  public String GetErrSymbolName() { return mErrSymbolName ; } // GetErrSymbolName()
} // class UnboundSymbol

class AttemptToApplyNonFunction extends Exception {
  private TreeNode mErrNode ;
  public AttemptToApplyNonFunction( TreeNode node ) { mErrNode = node ; } // AttempToApplyNonFunction()

  public TreeNode GetErrNdoe() { return mErrNode ; } // GetErrCommandName()
} // class AttemptToApplyNonFunction

class LevelOfExit extends Exception {
} // class LevelOfExit

class DefineFormatException extends Exception {
} // class DefineFormatException

class CleanEnvNotRootException extends Exception {
} // class CleanEnvNotRootException

class IncorrectArgumentType extends Exception {
  private String mFuncName ;
  private TreeNode mErrNode ;
  public IncorrectArgumentType( String f, TreeNode n ) {
    mFuncName = f ;
    mErrNode = n ;
  } // IncorrectArgumentType()

  public String GetFuncName() { return mFuncName ; } // GetFuncName()

  public TreeNode GetErrNode() { return mErrNode ; } // GetErrNode()
} // class IncorrectArgumentType

class ParameterformatException extends Exception {
  private TreeNode mErrNode ;
  public ParameterformatException( TreeNode node ) { mErrNode = node ; } // ParameterformatException()

  public TreeNode GetErrNdoe() { return mErrNode ; } // GetErrCommandName()
} // class ParameterformatException

class NoReturnValue extends Exception {
  private String mErrNode ;
  public NoReturnValue( String node ) { mErrNode = node ; } // NoReturnValue()

  public String GetErrName() { return mErrNode ; } // GetErrCommandName()
} // class NoReturnValue

class LetFormatException extends Exception {
} // class LetFormatException

class LambdaFormatException extends Exception {
} // class LambdaFormatException